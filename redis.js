const port_redis = process.env.PORT || 6378;
module.exports = require('redis').createClient(port_redis);
