
//set up dependencies
const express = require("express");
const axios = require("axios");
const bodyParser = require("body-parser");


//routes
const usersRouter = require('./routes/users');
const validateRouter = require('./routes/validate');



//configure redis client on port 6379
// const redis_client = redis.createClient(port_redis);

//configure express server
const app = express();

//Body Parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.all('*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

//listen on port 5000;
app.use('/users', usersRouter);
app.use('/validate', validateRouter);
module.exports = app
