# watermelon-test-back

back for test watermelon

## Installation

```bash
npm install
```

## Usage
```bash
npm start
```

## API
If need change redis port -> go to redis.js file

## Test
```bash
npm test
```

```javascript
POST: http://localhost:5000/users/create
Body: {
	"email": "xxxx@gmail.com",
	"password": "123456"
}
POST: http://localhost:5000/users/login
Body:{
	"email": "xxxx@gmail.com",
	"password": "123456"
}
POST: http://localhost:5000/validate/validateToken
Header:{
	"Authorization": TOKEN_GENERATE_IN_LOGIN
}

```

## Jest/Supertest with Redis Problem
There is a problem with Jest/Supertest when Redis apply a setex or set. You can solve this adding this command in redis-cli
```bash
config set stop-writes-on-bgsave-error no
```
