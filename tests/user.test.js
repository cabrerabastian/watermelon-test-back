const request = require('supertest');
const app = require('../index');
const user = require('./../__mockData__/user.json');
const rickMortyService = require('../services').rickAndMortyService;

describe('Test API user', () => {
  describe('Create new user', () => {
    it('should create a new user > user class', async () => {
      const res = await request(app)
        .post(`/users/create`)
        .send(user)
      expect(res.statusCode).toBeOneOf([201, 400])
      //expect(res.body).toHaveProperty('error', false)
    })
  })
  describe('Login user', () => {
    it('should login user and return rick and morty characters > user class', async () => {
      const res = await request(app)
        .post(`/users/login`)
        .send(user)
      expect(res.statusCode).toEqual(200)
      expect(res.body).toHaveProperty('error', false)
      expect(res.body).toHaveProperty('dataRickAndMorty')
    })
  })
})

describe('Rick Morty Data Service', () => {
  it('should get list rick and Morty', async () => {
    const dataResponse = await rickMortyService.getDataRickAndMorty();
    expect(dataResponse).toHaveProperty('error', false)
    expect(dataResponse).toHaveProperty('dataRickAndMorty')
  })
});
