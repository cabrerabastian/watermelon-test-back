const request = require('supertest');
const app = require('../index');
const user =  require('./../__mockData__/user.json');

const headers = { 'Authorization': user.token, Accept: 'application/json' };
describe('Validate Token', () => {
  it('should validate token sended', async () => {
    const res = await request(app)
      .post(`/validate/validateToken`)
      .send()
      .set(headers)
    expect(res.statusCode).toBeOneOf([201, 401])
  })
})