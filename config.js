/* This file its for general config in the system */

module.exports = {
  saltRounds: 2,
  jwtSecret: 'watermelon',
  tokenExpireTime: '2m',
  urlRickAndMorty: 'https://rickandmortyapi.com/api/character/',
  urlTestLocal: 'http://localhost:5000'
}