'use strict';
const bcrypt = require('bcryptjs');
const config = require('../config');
const jwt = require('jsonwebtoken');
const rickMortyService = require("../services").rickAndMortyService;
const redisService = require("../services").redisService;

class UserController {
  constructor() {
  }
  create(req, res) {
    //Body data
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).json({
        error: true,
        message: 'Problemas con los parametros ingresados'
      })
    } else {
      //Check if exists
      redisService.getInRedis(email, () => async (err, data) => {
        if (err) {
          return res.status(400).json({
            error: true,
            message: err
          })
        }
        //exists no create
        if (data) {
          return res.status(400).json({
            error: true,
            message: 'El email ya se encuentra registrado.'
          })
        } else {
          // Create
          //redis.setex(email, 3600, JSON.stringify({ password: bcrypt.hashSync(password, config.saltRounds) }));
          await redisService.setInRedis(email, 3600, JSON.stringify({ password: bcrypt.hashSync(password, config.saltRounds) }));
          return res.status(201).json({
            error: false,
            message: 'Usuario creado correctamente'
          });
        }
      });
    }
  }

  login(req, res) {
    const { email, password } = req.body;
    if (!email || !password) {
      return res.status(400).json({
        error: true,
        message: 'No se ha ingresado email ó contraseña'
      })
    }
    //Check if exists
    redisService.getInRedis(email, () => async (err, user) => {
      if (err) {
        return res.status(400).json({
          error: true,
          message: err
        })
      }

      //if exists
      if (user) {
        const userObject = JSON.parse(user);
        if (!bcrypt.compareSync(password || '', userObject.password)) {
          return res.status(401).json({
            error: true,
            message: 'Contraseña incorrecta'
          })
        }
        const payload = {
          email,
          time: new Date()
        };
        const token = jwt.sign(payload, config.jwtSecret, {
          expiresIn: config.tokenExpireTime
        });
        // call to service
        const dataResponse = await rickMortyService.getDataRickAndMorty();
        if (dataResponse.error) {
          return res.status(400).json({
            error: true,
            message: dataResponse.message
          })
        }
        return res.status(200).json({
          error: false,
          email,
          token: token,
          dataRickAndMorty: dataResponse.dataRickAndMorty
        })
      } else {
        return res.status(404).json({
          error: true,
          message: 'Email no registrado'
        })
      }
    });
  }
}
module.exports = UserController;


