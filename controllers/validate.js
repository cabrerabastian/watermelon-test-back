
'use strict';
const rickMortyService = require("../services").rickAndMortyService;
class ValidateController {
  constructor() {
  }
  //Create new user
  async validateToken(req, res) {
    //if Pass to here its ok
    // call to service
    const dataResponse = await rickMortyService.getDataRickAndMorty();
    if (dataResponse.error) {
      return res.status(400).json({
        error: true,
        message: dataResponse.message
      })
    }
    return res.status(200).json({
      error: false,
      dataRickAndMorty: dataResponse.dataRickAndMorty
    })
  }
}

module.exports = ValidateController;



