const UserController = require('./user');
const ValidateController = require('./validate');

module.exports = {
  UserController,
  ValidateController
};