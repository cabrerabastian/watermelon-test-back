var express = require('express');
var router = express.Router();
const userController = require('../controllers').UserController;
const userControllerObj = new userController();
// const authMiddleware = require('../middlewares').auth;

/** GET TODO */

/* POST todo. */
router.post('/create', userControllerObj.create);
router.post('/login', userControllerObj.login);

module.exports = router;