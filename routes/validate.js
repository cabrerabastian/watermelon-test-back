var express = require('express');
var router = express.Router();
const ValidateController = require('../controllers').ValidateController;
const ValidateControllerObj = new ValidateController();

const authMiddleware = require('../middlewares').auth;

/** GET TODO */

/* POST todo. */
router.post('/validateToken', authMiddleware.checkAuth, ValidateControllerObj.validateToken);

module.exports = router;