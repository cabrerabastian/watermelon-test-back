const jwt = require('jsonwebtoken');
const config = require('../config');
const checkAuth = (req, res, next) => {
  const token = req.headers['authorization'];
  if (!token)
    return res.status(401).send({ error: true, auth: false, message: 'No se ha enviado token para autenticar.' });

  jwt.verify(token, config.jwtSecret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ auth: false, message: 'Problemas con validar el token', error: err.name });
    }
    req.user = {
      login: decoded.login,
      id: decoded.id
    };
    next();
  });
  //next();
}
module.exports = {
  checkAuth
}