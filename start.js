const app = require('./index.js');
const redis = require('./redis');

//setup port constants
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on Port ${port}`));
