const rickAndMortyService = require('./rickMorty.service');
const redisService = require('./redis.service');


module.exports = {
  rickAndMortyService,
  redisService
};