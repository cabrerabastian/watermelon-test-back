const redis = require('../redis');

const setInRedis = async (index, time = 3600, value) => {
  return redis.setex(index, time, value);
}

const getInRedis = (field, callback) => {
  return redis.get(field, callback());
}
module.exports = {
  setInRedis,
  getInRedis
};