
const axios = require("axios");
const config = require('../config');

const getDataRickAndMorty = async () => {
  return axios.get(config.urlRickAndMorty).then((responseRickAndMorty) => {
    //get data from response
    const dataRickAndMorty = responseRickAndMorty.data.results.map(character => {
      return {
        name: character.name,
        status: character.status,
        species: character.species,
        gender: character.gender,
        image: character.image
      }
    });
    return {
      error: false,
      dataRickAndMorty
    }
  }).catch((error) => {
    return {
      error: true,
      message: `Problemas con solicitud de Rick and Morty ${error}`
    }
  });
}

module.exports = {
  getDataRickAndMorty
};